# How can I help nature?

- 🍊 Eating very few to no meat, buying local (at country-scale) products
- 🚴 Car-less, use shared transports, a bike, ...
- 🔧 No resources waste (no pets, careful with electricity, water, less plastics packaging .., reuse, repair rather than buy)
- 🌳 Planting trees occasionally anywhere
